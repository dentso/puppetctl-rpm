%define _pkgroot /opt/infra-tools

Name:           puppetctl-rpm
Version:        1.0.0
Release:        1%{?dist}
Summary:        rpm wrapper for puppetctl
Group:          System
License:        GPLv2
URL:            n/a
Source0:        https://bitbucket.org/dentso/%{name}/get/%{name}-%{version}.tar.bz2
BuildArch:      noarch
Requires:       /usr/bin/puppet

%description
%{summary}

%prep

%build

%install
mkdir -p %{buildroot}
tar -C %{buildroot} --strip-components=1 -xvf %{S:0}
pushd %{buildroot}
rm -rf .git %{name}.spec LICENSE README.md make_rpm.sh
popd

%files
%attr(755, root, root) %{_pkgroot}/bin/*


%changelog
* Mon Jun 27 2016 - Esaie Vermilus <esaie.vermilus@hotschedules.com> - 1.0.0-1
- Initial RPM release

