#!/bin/bash
#
# wrapper script to disable the puppet agent
# It checks if there is a lock file. If  a lockfile is found,
# it will  will wait and retry again. It then tries to disable the
# puppet agent and creates a at job to re-enable it.
#
#

whoami_cmd="/usr/bin/whoami"
puppet_cmd="/usr/bin/puppet"
at_cmd="/usr/bin/at"
atq_cmd="/usr/bin/atq"
date_cmd="/bin/date"
progname=$0

opt_days=0
opt_hours=0
opt_mins=0
opt_msg=""
opt_user=""
max_mins=$(( 10*24*60 )) # 10 days
min_mins=5               # 5 mins


check_cmd() {
  linux_cmd=$1
  if [ ! -x "${linux_cmd}" ]; then
    echo "ERROR: Exec ("${linux_cmd}") not found."
    exit 1
  fi
}

check_cmd ${whoami_cmd}
check_cmd ${puppet_cmd}
check_cmd ${at_cmd}
check_cmd ${atq_cmd}
check_cmd ${date_cmd}

if [ "`${whoami_cmd}`" != "root" ]; then
  echo "ERROR: $progname should run as superuser"
  exit 1
fi

usage() {
  echo "Usage: $progname [-u user] [-d <days>] [-h <hours>] [-m <mins>] [-k <string>]" 1>&2
  echo "Total maintenance window duration in mins: ${min_mins}m < MW < ${max_mins}m"
  echo "Options:"
  echo "       -d: number of days (digit)"
  echo "       -h: number of hours (digit)"
  echo "       -m: number of minutes (digit)"
  echo "       -k: message (string in single quotes)"
  echo "       -u: user running the job (string in single quotes)"
  exit 1
}

count=`${atq_cmd} | wc -l | sed "s/ //g"`
if [ $count -gt 0 ]; then
  echo "ERROR: Previous at jobs were found. Exiting."
  ${atq_cmd}
  exit 1
fi

isNum() {
  myNumber=$1
  re='^[0-9]+$'
  if ! [[ $myNumber =~ $re ]] ; then
    echo "error: $myNumber is not a number" >&2
    return 1
  fi
  return 0
}

get_options() {
  arg=$*
  while getopts "d:h:m:k:u:" arg; do
    case $arg in
      d) opt_days=$OPTARG
         ( ! isNum $opt_days ) && usage
         ;;
      h) opt_hours=$OPTARG
         ( ! isNum $opt_hours ) && usage
         ;;
      m) opt_mins=$OPTARG
         ( ! isNum $opt_mins ) && usage
         ;;
      k) opt_msg=$OPTARG
         ;;
      u) opt_user=$OPTARG
         ;;
      *) usage
         ;;
    esac
  done
  shift $((OPTIND-1))
}

get_options $*
total_mins=$(( 1*${opt_mins} + 60*${opt_hours} + 60*24*${opt_days} ))
[ "${total_mins}" = "0" ] && usage
[ "${total_mins}" -lt "${min_mins}" ] && usage
[ "${total_mins}" -gt "${max_mins}" ] && usage

disabled_lockfile=`${puppet_cmd} agent --configprint agent_disabled_lockfile 2>/dev/null`
if [ -n "${disabled_lockfile}" -a -f "${disabled_lockfile}" ]; then
  echo "ERROR: disabled_lockfile found. Exiting."
  exit $retval
fi

at_cmd="${at_cmd} now + ${total_mins} minutes"
echo "Disabling the puppet agent"
date_time=`${date_cmd} '+%m%d%y %H:%M'`
sched_msg="${opt_msg} ${opt_user}: ${date_time} for ${opt_days}d-${opt_hours}h-${opt_mins}m"
${puppet_cmd} agent --disable "Disabled by $progname: ${sched_msg}"
echo "Creating schedule for re-enabling the puppet agent: ${sched_msg}"
echo "${puppet_cmd} agent --enable" | ${at_cmd}

exit $?
