#!/bin/bash

echo -n "Enter bitbucket username: "
read bitbucketuser
echo -n "Enter bitbucket password: "
read -s password

branch=master
#bitbucketuser=dentso
appname=puppetctl-rpm
rm -rf ~/{rpmbuild}
#git clone git@bitbucket.org:dentso/${appname}.git
cd ; mkdir -p ~/rpmbuild/{BUILDROOT,BUILD,SOURCES,SRPMS,RPMS,SPECS}
cd ~/rpmbuild/SOURCES

#retrieves the latest source files
wget --user=${bitbucketuser} --password=${password} https://bitbucket.org/dentso/${appname}/get/${branch}.tar.bz2
[ $? != 0 ] && exit 1
version=`egrep -i "^version:"  ~/${appname}/${appname}.spec | cut -d: -f2 | sed "s/ //g"`
mv ${branch}.tar.bz2 ${appname}-${version}.tar.bz2
cat ${appname}-${version}.tar.bz2 | bunzip2 | tar xvf -
cp *${appname}*/*.spec ~/rpmbuild/SPECS
rpmbuild -bb -v ~/rpmbuild/SPECS/${appname}.spec
exit $?
